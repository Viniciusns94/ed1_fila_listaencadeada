/*Lista 2->ex.(2) Implemente filas duplas utilizando lista duplamente encadeada*/

# include <stdio.h>
# include <stdlib.h>

typedef struct no{
	int info;
	struct no *prox;
} *ptno;

void insere (ptno *P, int valor){
	ptno aux =(ptno) malloc (sizeof(struct no));
	if(!aux) 
		puts("Memória Cheia !");	
	else{
		aux->info = valor;
		if(*P){
			puts("aqui");
			aux->prox = (*P)->prox;
			(*P)->prox = aux;
		}
		else
			aux->prox = aux;
		*P = aux;
	}	
}

void retira(ptno *P, int *valor){
	if(!(*P)) 
		puts("Fila Vazia");
	else{
		ptno aux = (*P)->prox;
		*valor = aux->info;
		if(aux == (*P))
			*P = NULL;
		else
			(*P)->prox = aux->prox;
		free(aux);
	}
}

void mostra(ptno P){
	ptno aux = P;
	printf("Fila = {");
	if(P){
		do{
			aux = aux->prox;
			printf("%d ", aux->info);
		}while(aux != P);
	}
	printf("}\n");
}

void main (){
	ptno Fila = NULL;
	int valor;
	insere(&Fila, 10);
	insere(&Fila, 20);
	insere(&Fila, 30);

	mostra(Fila);

	retira(&Fila, &valor);
	printf("Retira: %d\n", valor);
	mostra(Fila);
	retira(&Fila, &valor);
	printf("Retira: %d\n", valor);
	mostra(Fila);
	retira(&Fila, &valor);
	printf("Retira: %d\n", valor);
	mostra(Fila);
}